<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Module;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::create([
            'course' => 'Desarrollo de Aplicaciones Web',
            'name' => 'Desarrollo Web entorno de servidor',
            'code' => '0613',
            'short_name' => 'Servidor',
            'abreviation' => 'DWES'
        ]);

        Module::create([
            'course' => 'Desarrollo de Aplicaciones Web',
            'name' => 'Desarrollo Web entorno de cliente',
            'code' => '0612',
            'short_name' => 'Cliente',
            'abreviation' => 'DWEC'
        ]);

        Module::create([
            'course' => 'Desarrollo de Aplicaciones Web',
            'name' => 'Desarrollo de Interfaces Web',
            'code' => '0615',
            'short_name' => 'Interfaces',
            'abreviation' => 'DIW'
        ]);
        Module::create([
            'course' => 'Desarrollo de Aplicaciones Web',
            'name' => 'Despliegue de Aplicaciones Web',
            'code' => '0614',
            'short_name' => 'Despliegue',
            'abreviation' => 'DAW'
        ]);
    }
}
