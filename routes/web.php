<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PruebaController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\StudyController;
use App\Http\Controllers\ModuleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('prueba', function () {
echo "hola mundo";
});

Route::get('/prueba2', [PruebaController::class, 'hola'] );
Route::get('/prueba2/{name}', [PruebaController::class, 'saludoCompleto'] );
Route::get('/tabla/{size}', [PruebaController::class, 'tabla'] );

Route::resource('photos', PhotoController::class);
Route::resource('studies', StudyController::class);
Route::resource('modules', ModuleController::class);

Route::get('saludo', [PruebaController::class, 'saludo']);

// Route::get('photos/create', [PhotosController::class, 'create']);
// Route::get('photos/index', [PhotosController::class, 'index']);
//Route::get('photos/show', [PhotosController::class, 'show']);
// Route::get('photos/edit', [PhotosController::class, 'edit']);
// // Route::put('photos/{id}', 'PhotosController::class, update');
// // Route::delete('photos/{id}', 'PhotosController::class, destroy');
// las rutas en plural 
//tablas en plural
//controladores y modelos en singular