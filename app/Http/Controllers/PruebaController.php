<?php

namespace App\Http\Controllers;

use Facade\FlareClient\View;
use Illuminate\Http\Request;

class PruebaController extends Controller
{
    public function hola()
    {
        return ("hola desde pruebacontroller");
    }
    public function saludoCompleto($name)
    {
        return ("hola $name  desde pruebacontroller");
    }
    public function saludo()
    {
        echo "saludado estas";
    }
    public function tabla($size)
    {

        return view('prueba.tabla',[
            'size'=>$size
        ]);
    }
}
