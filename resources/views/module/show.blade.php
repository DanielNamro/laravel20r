<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Modulo nº {{$module->id}}</h1>

    <ul>
        <li>
        <strong>ID</strong>
        {{$module->id}}
        </li>
        <li>
        <strong>COURSE</strong>
        {{$module->course}}
        </li>
        <li>
        <strong>NAME</strong>
        {{$module->name}}
        </li>
        <li>
        <strong>CODE</strong>
        {{$module->code}}
        </li>
        <li>
        <strong>SHORT NAME</strong>
        {{$module->short_name}}
        </li>
        <li>
        <strong>ABREVIATION</strong>
        {{$module->abreviation}}
        </li>
    </ul>
</body>
</html>