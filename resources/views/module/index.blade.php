<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <h1>Lista de Modulos</h1>
    <table border="1px">
        <tr>
            <th>ID</th>
            <th>COURSE</th>
            <th>NAME</th>
            <th>CODE</th>
            <th>SHORT NAME</th>
            <th>ABREVIATION</th>
        </tr>

        @forelse ($modules as $modul)
        <tr>
            <td>{{$modul->id}}</td>
            <td>{{$modul->course}}</td>
            <td>{{$modul->name}}</td>
            <td>{{$modul->code}}</td>
            <td>{{$modul->short_name}}</td>
            <td>{{$modul->abreviation}}</td>
            <td><a href="/modules/{{$modul->id}}"> ver </a></td>
        </tr>

        @empty
<tr>
    <td colspan="3">No hay modulos registrados</td>
</tr>
        @endforelse
    </table>
</body>

</html>